

var app = angular.module("myApp", ["ngRoute"]);
app.config(function ($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl: "main.htm",
        })
        .when("/pacientes", {
            templateUrl: "pacientes.html",
            controller: "pacientesCtrl"
        })
        .when("/consulta", {
            templateUrl: "consulta.html",
            controller: "consultaCtrl"
        });
});


app.controller("consultaCtrl", function ($scope, $http, $filter) {
    $scope.listaEspera = [];

    $scope.init = function () {
        $scope.loadList();
    };

    $scope.releaseConsultations = function () {
        $http({
            method: 'GET',
            url: "/api/consultation/release-all"
        }).then(function (success) {
            $scope.loadList();
        }, function (error) {
            console.log(error)

        });
    };

    $scope.generateConsultations = function () {
        $http({
            method: 'GET',
            url: "/api/consultation/generateRandom"
        }).then(function (success) {
            $scope.loadList();
        }, function (error) {
            console.log(error)

        });
    };

    $scope.filterByPatients = function () {

        $scope.isFiltered = true;
        $scope.consultationList = $filter('orderBy')(angular.copy($scope.originalConsultationList), 'totalPatients', true);
    };

    $scope.removeFilters = function () {

        $scope.isFiltered = false;
        $scope.consultationList = angular.copy($scope.originalConsultationList);
    };


    $scope.loadList = function () {
        $http({
            method: 'GET',
            url: "/api/consultation"
        }).then(function (success) {

            $scope.originalConsultationList = angular.copy(success.data.data);
            $scope.consultationList = $filter('orderBy')($scope.originalConsultationList, 'id');
            if ($scope.consultationList == null || $scope.consultationList.length < 1) {
                $scope.emptyConsultation = true;
            }

        }, function (error) {
            console.log(error)

        });

    }

    $scope.init();

});


app.controller("pacientesCtrl", function ($scope, $http, $filter) {

    $scope.optimized = false;

    $scope.init = function () {

        $scope.listaEspera = [];

        $http({
            method: 'GET',
            url: "/api/wait-record"
        }).then(function (success) {
            console.log(success)
            $scope.listaEspera = angular.copy(success.data.data);
            $scope.listaEsperaActual = $filter('orderBy')($scope.listaEspera, 'id');
        }, function (error) {
            console.log(error)

        });

    }

    $scope.init();

    $scope.searchWorst = function () {


        var x = $filter('filter')($scope.listaEspera, { 'patient': { 'record': $scope.recordSearch } })[0];

        var listaEsperaActual = [];
        for (var i = 0; i < $scope.listaEspera.length; i++) {

            var element = $scope.listaEspera[i];

            if (element.record != $scope.recordSearch && element.risk > x['risk']) {
                listaEsperaActual.push(element);
            }

        }

        if (listaEsperaActual.length > 0) {
            $scope.listaEsperaActual = $filter('orderBy')(listaEsperaActual, ['risk'], true);
            $scope.isWorstSearch = true;
        } else {
            alert("sin resultados");
            $scope.recordSearch = "";

        }

    }

    $scope.removeFilters = function () {
        $scope.showform = false;
        $scope.smokerList = false;
        $scope.elderList = false;
        $scope.exito = false;
        $scope.error = false;
        $scope.isWorstSearch = false;
        $scope.listaEsperaActual = $filter('orderBy')($scope.listaEspera, 'id');
    }

    $scope.toggleOptimization = function () {

        $scope.optimized = !$scope.optimized;
        var state = $scope.optimized ? 1 : 0;

        $http({
            method: 'PUT',
            url: "/api/optimize",
            data: state
        }).then(function (success) {
            $scope.init();
        }, function (error) {
            $scope.init();
            console.log(error)

        });


    }

    $scope.smokersFilter = function () {

        if (!$scope.smokerList) {
            $scope.listaEsperaActual = angular.copy($scope.listaEspera);
            return;
        }

        var minAge = 16;
        var maxAge = 40;

        var espAttr = 1;
        var priority = 4;

        var listaEsperaActual = [];
        for (var i = 0; i < $scope.listaEspera.length; i++) {

            var element = $scope.listaEspera[i];

            if (element.age >= minAge && element.age <= maxAge &&
                element.espAttr > 0 && element.priority > priority) {
                listaEsperaActual.push(element);
            }

        }
        $scope.listaEsperaActual = $filter('orderBy')(listaEsperaActual, ['risk', 'priority']);

    }

    $scope.elderFilter = function () {

        if (!$scope.elderList) {
            $scope.listaEsperaActual = angular.copy($scope.listaEspera);
            return;
        }

        var minAge = 41;

        var espAttr = 1;
        var priority = 4;

        var anciano = $filter('orderBy')($scope.listaEspera, ['age'], true)[0];
        $scope.listaEsperaActual = new Array(anciano);

    }

    $scope.addPatient = function () {
        $http({
            method: 'POST',
            url: "/api/patient",
            data: $scope.patient
        })
            .then(function (success) {
                console.log("Exito");
                $scope.exito = true;
                $scope.patient = {};
                $scope.showForm = false;
                $scope.init();
            }, function (error) {
                console.log("error");
                $scope.error = true;
                $scope.showForm = false;
            });

    };


    $scope.range = function (n) {
        return new Array(n);
    };


});
