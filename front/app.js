const express = require('express');
const app = express();
var httpO = require('http');
var http = require('http').Server(app);
var httpProxy = require('http-proxy');
var io = require('socket.io')(http);
var request = require('request');


io.on('connection', function (socket) {
  console.log('a user connected');
  socket.on('disconnect', function () {
    console.log('user disconnected');
  });

  socket.on('msj', function (msg) {
    console.log('message: ' + msg);
    socket.broadcast.emit('hiback');
    io.emit('msj', "CCS mm");

  });


});

app.use('/static', express.static('public'));

app.get('/updateFront', function (req, res) {

  io.emit('msj', 'actualizar consulta');
  res.send('ok');
})


http.listen(3000, function () {
  console.log('listening on *:3000');
});


/**
 * Proxy 
 */
var CNTX = {
  "static": 'http://node:3000',
  "api": 'http://backend:8080'
};

var proxy = httpProxy.createProxyServer({});

var server = httpO.createServer(function (req, res) {

  var targ = CNTX[getCntx(req.url)]
  if (targ === undefined)
    return res;
  try {
    proxy.web(req, res, {
      target: CNTX[getCntx(req.url)]
    });
  } catch (err) {

  }
});

console.log("listening on port 5050")
server.listen(5050);


function getCntx(url) {
  var indexFin = url.substr(1).indexOf("/");
  return url.substr(1, indexFin);
}