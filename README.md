# Test

La prueba enviadad fue realizada con las siguientes tecnologías

  - Servidor statico + proxy nodeJS
  - Base de datos mysql
  - Servicios rest con SpringBoot 
  - Docker para el ambiente

Todo el ambiente se encuentra dockerizado, por lo que **se debe tener instalado docker y docker-compose**
Para correr el proyecto se debe  clonar el repositorio y ejecutar desde la carpeta raiz
**docker-compose up** y se debe esperar a que el proyecto descargue todas las dependencias por primera vez **(2 min)**

Luego se debe ir a la url [http://localhost:5050/static/#!/](http://localhost:5050/static/#!/)
### Servidores
  - Proxy localhost:5050
  - Node localhost:3000
  - rest localhost:8080

Si tiene alguna duda por favor comunicarse con Daniel Ruiz daruizg@gmail.com




