package cl.fonasa.patient.attention.model;

import javax.persistence.*;

/**
 * Entity of type patient 
 * @author druiz
 *
 */
@Entity
@Table(name = "patient") 
public class Patient {

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	private Long id;

	private String name;

	@Column(unique = true)
	private String rut;

	private String record;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public String getRecord() {
		return record;
	}

	public void setRecord(String record) {
		this.record = record;
	}
}


