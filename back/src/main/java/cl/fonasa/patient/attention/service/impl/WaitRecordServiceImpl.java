package cl.fonasa.patient.attention.service.impl;

import cl.fonasa.patient.attention.model.Consultation;
import cl.fonasa.patient.attention.model.WaitRecord;
import cl.fonasa.patient.attention.repository.ConsultationRepository;
import cl.fonasa.patient.attention.repository.OptimizationRepository;
import cl.fonasa.patient.attention.repository.WaitRecordRepository;
import cl.fonasa.patient.attention.service.WaitRecordService;
import cl.fonasa.patient.attention.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

@Service
public class WaitRecordServiceImpl implements WaitRecordService {

    public static final int MAX_WAITING = 10;

    @Autowired
    WaitRecordRepository waitRecordRepository;

    @Autowired
    ConsultationRepository consultRepo;

    @Autowired
    OptimizationRepository optRepo;


    @Override
    public List<WaitRecord> listAll() {
        List<WaitRecord> wrl = new LinkedList<>();

        Iterator it= waitRecordRepository.getAllByNormalOrder().iterator();

        while (it.hasNext()) {
            wrl.add((WaitRecord) it.next());
        }

        return wrl;
    }

    @Override
    public void sendPatientsToConsultation(List<Consultation> consultations) {


        for (Consultation c : consultations) {
            List<WaitRecord> wrl = listAll();
            for (WaitRecord wr : wrl) {
                if (Utils.checkPatientRules(c.getName(), wr)) {
                    waitRecordRepository.delete(wr);
                    c.setState(false);
                    consultRepo.save(c);
                    Utils.updateFront();
                    break;
                }
            }
        }
    }
}