package cl.fonasa.patient.attention.model;

import javax.persistence.*;
import javax.persistence.JoinColumn;

@Entity
@Table(name = "consultation")
public class Consultation {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    public Long id;

    @Enumerated(EnumType.STRING)
    private Consultation.CS name;

    public String specialist;

    public int totalPatients;
    @Column(columnDefinition="tinyint(1) default 0")
    public boolean state;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSpecialist() {
        return specialist;
    }

    public void setSpecialist(String specialist) {
        this.specialist = specialist;
    }

    public int getTotalPatients() {
        return totalPatients;
    }

    public void setTotalPatients(int totalPatients) {
        this.totalPatients = totalPatients;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public CS getName() {
        return name;
    }

    public void setName(CS name) {
        this.name = name;
    }

    public enum CS {
        PEDIATRIA, URGENCIA,CGI
    }



}