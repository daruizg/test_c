package cl.fonasa.patient.attention.service.impl;

import cl.fonasa.patient.attention.model.Consultation;
import cl.fonasa.patient.attention.repository.ConsultationRepository;
import cl.fonasa.patient.attention.service.ConsultationService;
import cl.fonasa.patient.attention.service.WaitRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

@Service
public class ConsultationServiceImpl implements ConsultationService {

    public static final int MAX_WAITING=10;

    @Autowired
    ConsultationRepository cr;

    @Autowired
    WaitRecordService wrs;


    @Override
    public List<Consultation> listConsultations() {
        List<Consultation> cl=new LinkedList<>();
        Iterator it=cr.findAll().iterator();
        while (it.hasNext()) {
            cl.add((Consultation) it.next());
        }

        return cl;
    }

    @Override
    public void generateConsultations() {



        Random rand = new Random();

        for (int i = 0; i < 10; i++) {

            int  n = rand.nextInt(3) ;
            Consultation cons=new Consultation();
            cons.setName(Consultation.CS.values()[n]);
            cons.setSpecialist("Especialista "+i+n);
            cons.setState(false);
            cons.setTotalPatients(0);

            cr.save(cons);
            System.out.println("guardando 1 ");
        }



    }

    @Override
    public void setAllFree() {
        cr.setAllFree(1L);
        wrs.sendPatientsToConsultation(listAvailableConsultation());
    }

    @Override
    public void setFree(Long id) {
        cr.setFree(id);
    }


    @Override
    public List<Consultation> listAvailableConsultation() {
        List<Consultation> cl=new LinkedList<>();
        Iterator it=cr.getAvailable().iterator();
        while (it.hasNext()) {
            cl.add((Consultation) it.next());
        }

        return cl;
    }

}
