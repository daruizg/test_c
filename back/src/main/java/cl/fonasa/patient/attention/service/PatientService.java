package cl.fonasa.patient.attention.service;

import cl.fonasa.patient.attention.io.PatientIo;
import cl.fonasa.patient.attention.model.Consultation;
import cl.fonasa.patient.attention.model.Patient;

import java.util.List;

/**
 * 
 * @author druiz
 *
 */

public interface PatientService {

	boolean patientRegitration(PatientIo patient);

	List<Patient> listPatients();


}
