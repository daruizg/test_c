package cl.fonasa.patient.attention.controller;

import cl.fonasa.patient.attention.model.Consultation;
import cl.fonasa.patient.attention.service.ConsultationService;
import cl.fonasa.patient.attention.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cl.fonasa.patient.attention.io.GenericResponse;

import java.util.List;


@RestController
@RequestMapping("/api/consultation")
public class ConsultationController {

	@Autowired
	ConsultationService cs;
	
	/**
	 * Get a list of all available consultations
	 * @return GenericResponse
	 */
	@RequestMapping(value="/z",method=RequestMethod.GET)
	public ResponseEntity<GenericResponse> xxxx(){
		Utils.updateFront();
		return new ResponseEntity<GenericResponse>(new GenericResponse(), HttpStatus.OK);
	}
	@RequestMapping(value="",method=RequestMethod.GET)
	public ResponseEntity<GenericResponse> getAll(){
		GenericResponse response = new GenericResponse();
		HttpStatus status = HttpStatus.OK;
		try {
			List<Consultation> lista=cs.listConsultations();

			response.setCode(HttpStatus.OK.toString());
			response.setDesciption("Exitoso");
			response.setMessage("exitoso");
			response.setData(lista);


		} catch (Exception e) {
			response.setCode(HttpStatus.INTERNAL_SERVER_ERROR.toString());
			response.setDesciption("ERROR");
			response.setMessage(e.getMessage());
			status = HttpStatus.INTERNAL_SERVER_ERROR;
		}

		return new ResponseEntity<GenericResponse>(response, status);
	}
	
	
	@RequestMapping(value="/generateRandom",method=RequestMethod.GET)
	public ResponseEntity<GenericResponse> generateRandom(){
		cs.generateConsultations();
		return new ResponseEntity<GenericResponse>(new GenericResponse(), HttpStatus.ACCEPTED);
	}

	@RequestMapping(value="/release-all",method=RequestMethod.GET)
	public ResponseEntity<GenericResponse> updateAll(){

		GenericResponse response = new GenericResponse();
		HttpStatus status = HttpStatus.OK;

		try {
			cs.setAllFree();
			response.setCode(HttpStatus.OK.toString());
			response.setDesciption("Exitoso");
			response.setMessage("exitoso");


		} catch (Exception e) {
			response.setCode(HttpStatus.INTERNAL_SERVER_ERROR.toString());
			response.setDesciption("ERROR");
			response.setMessage(e.getMessage());
			status = HttpStatus.INTERNAL_SERVER_ERROR;
		}

		return new ResponseEntity<>(response, status);
	}


	@RequestMapping(value="/",method=RequestMethod.PUT)
	public ResponseEntity<GenericResponse> update(@RequestBody Long id){

		GenericResponse response = new GenericResponse();
		HttpStatus status = HttpStatus.OK;

		try {
			cs.setFree(id);
			response.setCode(HttpStatus.OK.toString());
			response.setDesciption("Exitoso");
			response.setMessage("exitoso");


		} catch (Exception e) {
			response.setCode(HttpStatus.INTERNAL_SERVER_ERROR.toString());
			response.setDesciption("ERROR");
			response.setMessage(e.getMessage());
			status = HttpStatus.INTERNAL_SERVER_ERROR;
		}

		return new ResponseEntity<GenericResponse>(new GenericResponse(), HttpStatus.ACCEPTED);
	}



}
