package cl.fonasa.patient.attention.service.impl;

import cl.fonasa.patient.attention.io.PatientIo;
import cl.fonasa.patient.attention.model.Consultation;
import cl.fonasa.patient.attention.model.Patient;
import cl.fonasa.patient.attention.model.WaitRecord;
import cl.fonasa.patient.attention.repository.PatientRepository;
import cl.fonasa.patient.attention.repository.WaitRecordRepository;
import cl.fonasa.patient.attention.service.ConsultationService;
import cl.fonasa.patient.attention.service.PatientService;
import cl.fonasa.patient.attention.service.WaitRecordService;
import cl.fonasa.patient.attention.util.Utils;
import cl.fonasa.patient.attention.util.Converter;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

@Service
public class PatientServiceImpl implements PatientService {

    public static final int MAX_WAITING = 10;

    @Autowired
    PatientRepository pr;

    @Autowired
    WaitRecordRepository wrr;

    @Autowired
    WaitRecordService wrs;

    @Autowired
    ConsultationService cs;


    @Override
    public boolean patientRegitration(PatientIo patient) {

        Patient newPatient = Converter.patientToIo(patient);

        if (pr.countByRut(patient.getRut())!=1) {
            pr.save(newPatient);
        }

        WaitRecord wr = new WaitRecord();

        wr.setPriority((Utils.getPriority(patient.getAge(), patient.getEspAttr())));
        wr.setPatient(newPatient);
        wr.setRisk(Utils.getRisk(patient.getAge(), wr.getPriority()));
        wr.setWaitingArea(WaitRecord.WA.ESPERA);
        wr.setAge(patient.getAge());
        wr.setEspAttr(patient.getEspAttr());
        if (wrr.countByWaitingArea(WaitRecord.WA.ESPERA) >= MAX_WAITING) {
            wr.setWaitingArea(WaitRecord.WA.PENDIENTE);
        }

        wrr.save(wr);

        wrs.sendPatientsToConsultation(cs.listAvailableConsultation());

        return true;
    }

    @Override
    public List<Patient> listPatients() {

        List<Patient> pl = new LinkedList<>();
        Iterator it = pr.findAll().iterator();
        while (it.hasNext()) {
            pl.add((Patient) it.next());
        }

        return pl;

    }


}
