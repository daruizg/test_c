package cl.fonasa.patient.attention.service;

import cl.fonasa.patient.attention.model.Consultation;
import cl.fonasa.patient.attention.model.Patient;
import cl.fonasa.patient.attention.model.WaitRecord;

import java.util.List;

/**
 * 
 * @author druiz
 *
 */

public interface WaitRecordService {

	List<WaitRecord> listAll();
	void sendPatientsToConsultation(List<Consultation> consultations);

}
