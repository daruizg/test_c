package cl.fonasa.patient.attention.controller;

import cl.fonasa.patient.attention.io.PatientIo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cl.fonasa.patient.attention.io.GenericResponse;

import cl.fonasa.patient.attention.service.PatientService;


@RestController
@RequestMapping(value = "/api/patient")
public class PatientController {

    @Autowired
    public PatientService ps;

    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<GenericResponse> registrar(@RequestBody PatientIo patient) {

        GenericResponse response = new GenericResponse();
        HttpStatus status = HttpStatus.OK;
        try {
            ps.patientRegitration(patient);
            response.setCode(HttpStatus.OK.toString());
            response.setDesciption("Exitoso");
            response.setMessage("exitoso");


        } catch (Exception e) {
            response.setCode(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            response.setDesciption("ERROR");
            response.setMessage(e.getMessage());
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }


        return new ResponseEntity<GenericResponse>(response, status);
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<GenericResponse> obtener() {


        GenericResponse response = new GenericResponse();
        HttpStatus status = HttpStatus.OK;
        try {
            response.setCode(HttpStatus.OK.toString());
            response.setDesciption("Exitoso");
            response.setMessage("exitoso");
            response.setData(ps.listPatients());
        } catch (Exception e) {
            response.setCode(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            response.setDesciption("ERROR");
            response.setMessage(e.getMessage());
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }


        return new ResponseEntity<GenericResponse>(response, HttpStatus.ACCEPTED);
    }

}
