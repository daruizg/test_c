package cl.fonasa.patient.attention.io;

import javax.persistence.*;

/**
 * Entity of type patient 
 * @author druiz
 *
 */

public class PatientIo {

	private Long id;
	
	private String name;

	private String rut;
	
	private String record;

	private Long age;

	private Long espAttr;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public String getRecord() {
		return record;
	}

	public void setRecord(String record) {
		this.record = record;
	}

	public long getAge() {
		return age;
	}

	public void setAge(long age) {
		this.age = age;
	}

	public Long getEspAttr() {
		return espAttr;
	}

	public void setEspAttr(Long espAttr) {
		this.espAttr = espAttr;
	}
}
