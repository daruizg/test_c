package cl.fonasa.patient.attention.io;

import java.io.Serializable;

/**
 * 
 * @author druiz
 *
 */
public class GenericResponse implements Serializable {
	

	private static final long serialVersionUID = 3408870838285886335L;
	
	public String code;
	public String message;
	public String desciption;
	public Object data;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getDesciption() {
		return desciption;
	}
	public void setDesciption(String desciption) {
		this.desciption = desciption;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	
}
