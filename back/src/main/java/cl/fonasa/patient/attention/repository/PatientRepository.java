package cl.fonasa.patient.attention.repository;

import cl.fonasa.patient.attention.model.Consultation;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cl.fonasa.patient.attention.model.Patient;

import java.util.Collection;

@Repository
public interface PatientRepository extends CrudRepository<Patient, Long> {

    @Query("SELECT count(p.id) FROM Patient p WHERE p.rut = :rut")
    int countByRut(@Param("rut") String rut);
}
