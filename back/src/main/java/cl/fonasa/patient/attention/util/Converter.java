package cl.fonasa.patient.attention.util;

import cl.fonasa.patient.attention.io.PatientIo;
import cl.fonasa.patient.attention.model.Patient;

public class Converter {
    
    
    public  static Patient patientToIo(PatientIo pio){
        Patient p=new Patient();
        p.setId(pio.getId());
        p.setName(pio.getName());
        p.setRecord(pio.getRecord());
        p.setRut(pio.getRut());

        return p;
        
    }
}