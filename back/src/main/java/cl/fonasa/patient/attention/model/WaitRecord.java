package cl.fonasa.patient.attention.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "wait_record")
public class WaitRecord {

    public enum WA {
        ESPERA, PENDIENTE
    }

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "rut")
    private Patient patient;

    private Long age;

    private Double priority;

    private Double risk;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", nullable = false)
    private Date created= new Date();

    @Enumerated(EnumType.STRING)
    private WA waitingArea;


    private Long espAttr;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Long getAge() {
        return age;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    public Double getPriority() {
        return priority;
    }

    public void setPriority(Double priority) {
        this.priority = priority;
    }

    public Double getRisk() {
        return risk;
    }

    public void setRisk(Double risk) {
        this.risk = risk;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public WA getWaitingArea() {
        return waitingArea;
    }

    public void setWaitingArea(WA waitingArea) {
        this.waitingArea = waitingArea;
    }

    public Long getEspAttr() {
        return espAttr;
    }

    public void setEspAttr(Long espAttr) {
        this.espAttr = espAttr;
    }
}
