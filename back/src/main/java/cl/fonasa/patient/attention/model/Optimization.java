package cl.fonasa.patient.attention.model;

import javax.persistence.*;

/**
 * Entity of type patient 
 * @author druiz
 *
 */
@Entity
@Table(name = "optimization")
public class Optimization {

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	public Long id;

	@Column(columnDefinition="tinyint(1) default 0")
	public boolean state;

	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}


