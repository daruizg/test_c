package cl.fonasa.patient.attention.dao;

import cl.fonasa.patient.attention.model.Patient;

public interface PatientDao {
	
	public boolean add(Patient patient);

}
