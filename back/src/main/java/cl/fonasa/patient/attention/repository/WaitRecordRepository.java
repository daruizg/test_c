package cl.fonasa.patient.attention.repository;

import cl.fonasa.patient.attention.model.WaitRecord;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface WaitRecordRepository extends CrudRepository<WaitRecord, Long> {

    Long countByWaitingArea(WaitRecord.WA espera);

    @Query("SELECT wr FROM WaitRecord wr ORDER BY id asc, priority desc ")
    Collection<WaitRecord> getAllByNormalOrder();


}
