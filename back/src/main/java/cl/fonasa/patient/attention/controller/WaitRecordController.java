package cl.fonasa.patient.attention.controller;

import cl.fonasa.patient.attention.io.GenericResponse;
import cl.fonasa.patient.attention.service.WaitRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/wait-record")
public class WaitRecordController {

    @Autowired
    WaitRecordService wrs;


    /**
     * Get a list of all available records
     * @return GenericResponse
     */
    @RequestMapping(value="",method= RequestMethod.GET)
    public ResponseEntity<GenericResponse> getAll(){
        GenericResponse response = new GenericResponse();
        HttpStatus status = HttpStatus.OK;
        try {
            response.setCode(HttpStatus.OK.toString());
            response.setDesciption("Exitoso");
            response.setMessage("exitoso");
            response.setData(wrs.listAll());
        } catch (Exception e) {
            response.setCode(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            response.setDesciption("ERROR");
            response.setMessage(e.getMessage());
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }


        return new ResponseEntity<GenericResponse>(response, HttpStatus.ACCEPTED);
    }

}