package cl.fonasa.patient.attention.repository;

import cl.fonasa.patient.attention.model.Consultation;
import cl.fonasa.patient.attention.model.Optimization;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Collection;

@Repository
public interface OptimizationRepository extends CrudRepository<Optimization, Long> {

    @Query("SELECT state FROM Optimization")
    int getOpt();

}
