package cl.fonasa.patient.attention.util;

import cl.fonasa.patient.attention.model.Consultation;
import cl.fonasa.patient.attention.model.WaitRecord;

import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Util class to maske generic operations
 */
public class Utils {

    public static Double getPriority(Long age, Long attr) {

        double ret;
        //niño
        if (age <= 5) {
            ret = attr + 3;
        } else if (age > 5 && age <= 12) {
            ret = attr + 2;
        } else if (age > 12 && age <= 15) {
            ret = attr + 1;
        }
        //adulto
        boolean isYoung = age > 15 && age <= 40;
        if (isYoung && attr > 0) {
            ret = (attr / 4) + 2;
        } else if (isYoung) {
            ret = 2;
        }

        //anciano
        if (age > 40 && attr > 0) {
            ret = (age / 20) + 4;
        } else {
            ret = (age / 30) + 3;
        }
        return ret;

    }

    public static double getRisk(long age, double priority) {
        double ret;
        if (age > 40) {
            return (age * priority) / 100 + 5.3;
        } else {
            return (age * priority) / 100;
        }

    }


    public static boolean checkPatientRules(Consultation.CS type, WaitRecord wr) {
        boolean ret=false;
        switch (type) {
            case PEDIATRIA:
                if(wr.getAge()<=15 || wr.getPriority()<=4){
                    ret= true;
                }


            case URGENCIA:
                if(wr.getPriority()>4){
                    ret=true;
                }

            case CGI:
                if(wr.getAge()>15){
                    ret=true;
                }

                break;
        }
        return  ret;
    }


    public  static void updateFront() {
        try {
            URL url = new URL("http://node:3000/updateFront");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setDoOutput(true);

            DataOutputStream out = new DataOutputStream(con.getOutputStream());
            out.writeBytes("");
            out.flush();
            out.close();
        }catch(Exception e ){
            e.printStackTrace();

        }

    }
}