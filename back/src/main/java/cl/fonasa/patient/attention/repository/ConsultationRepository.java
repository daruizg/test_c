package cl.fonasa.patient.attention.repository;

import cl.fonasa.patient.attention.model.Consultation;
import cl.fonasa.patient.attention.model.WaitRecord;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.List;

@Repository
public interface ConsultationRepository extends CrudRepository<Consultation, Long> {

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query(value = "update consultation c set c.state= ? ",
            nativeQuery = true)
    void setAllFree(Long state);


    @Modifying
    @Query("update Consultation c set c.totalPatients = 1 where c.id = :id")
    void setFree(@Param("id") Long id);

    @Query("SELECT c FROM Consultation c WHERE c.state = 1")
    Collection<Consultation> getAvailable();


}
